// Cole Grondy, CS 201, L01, 11/19/22, Pseudocode.java

// Step 1: Decide how large you want your square.  For best results, make it no smaller than 5 console lines in total.
// Step 2: For every line of code you make, make sure each line is equally distant from the left of the screen.
// Step 3: On the first line you want the square, make a System.out.println command and type out one spacebar press followed by a series of consecutive underscores.  This is the first side of your square.
// Step 4: the next line, make a System.out.println command and type out two "|" keys separated by enough spacebar presses so that each "|" gets as close as it can to the underscores without being directly under them.
// Step 5: Repeat the line of code made in step 4 until the "|" keys make approximately as long of a line as the line of underscores made in step 1.
// Step 6: On the final line of code you made, replace the spacebar presses in the System.out.println command with underscores, maintaining the original distance between the "|" keys.
// Step 7: Save and run the code.
package labs.lab0;

public class Pseudocode {

	public static void main(String[] args) {
		System.out.println(" ________");
		System.out.println("|        |");
		System.out.println("|        |");
		System.out.println("|        |");
		System.out.println("|________|");  // Each System.out.println command prints out a piece of a square.  When each command is equally distant from the left of the screen, the square that is going to appear in the console should look similar to how it appears in the code itself.


	}

}

// I got the desired result of making a square, to an extent.  There are a few issues with this method, primarily being unable to make a perfect square.  The console lines have a space between them, so any lines that exist on multiple console lines will always be fragmented.  Additionally, I was unable to get the corners of the square to line up perfectly.