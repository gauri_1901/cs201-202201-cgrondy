package labs.lab1;

import java.util.Scanner;

public class Temperature {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Creates scanner
		
		System.out.print("Enter the temperature in fahrenheit: ");
		double x = Double.parseDouble(input.nextLine()); // Stores input
		
		System.out.println("The temperature in celsius is " + ((x - 32) * (((double)5)/((double)9)))); // Formula for F to C uses input
		
		// Input: -40.  Expected output: -40.  Actual output: -40.0
		// Input: 23.2.  Expected output: -4.88.  Actual output: -4.888888888888889
		// Input: 212.  Expected output: 100.  Actual output: 100.0
		
		System.out.print("Enter the temperature in celcius: ");
		double y = Double.parseDouble(input.nextLine()); // Stores input
		
		System.out.println("The temperature in fahrenheit is " + ((((double)9)/((double)5)) * y + 32)); // Formula for C to F uses input
		
		// Input: -20.  Expected output: -4.  Actual output: -4.0
		// Input: 18.9.  Expected output: 66.02  Actual output: 66.02
		// Input: 200.  Expected output: 392.  Actual output: 392.0
		
		input.close(); // Close scanner
		


	}

}
