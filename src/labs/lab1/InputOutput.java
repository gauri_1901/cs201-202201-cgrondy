package labs.lab1;

import java.util.Scanner;

public class InputOutput {

	public static void main(String[] args) {
			Scanner input = new Scanner(System.in); // Creates scanner
			
			System.out.print("Enter your name: ");
			
			String value = input.nextLine(); // Stores input
			
			System.out.println("Echo: " + value);
			
			input.close(); // Close scanner

	}

}
